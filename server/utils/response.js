const addHeaders = (res, statusCode) => {
  return res.writeHead(statusCode, {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': false,
    'Access-Control-Allow-Headers': '*'
  })
}

module.exports.success = (res, data = null, statusCode = 200) => {
  addHeaders(res, statusCode)

  res.end(JSON.stringify({status: 'success', data}, null, 2))
}

module.exports.successWithCookie = (res, data = null, statusCode = 200) => {
  addHeaders(res, statusCode, data)

  res.end(JSON.stringify({status: 'success', data}, null, 2))
}

module.exports.error = (res, error = 'An unknown error occurred', statusCode = 500) => {
  addHeaders(res, statusCode)

  res.end(JSON.stringify({status: 'fail', error}, null, 2))
}

module.exports.validationError = (res, error = 'Data provided is not valid', statusCode = 400) => {
  addHeaders(res, statusCode)

  res.end(JSON.stringify({status: 'fail', error}, null, 2))
}
