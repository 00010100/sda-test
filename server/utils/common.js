const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')

const response = require('./response')

dotenv.config({path: './config.env'})

const signToken = id => {
  return jwt.sign({id}, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN
  })
}

module.exports.createSendToken = (res, user) => {
  const token = signToken(user.id)

  // Remove password from output
  user.password = undefined

  response.successWithCookie(res, {user, token})
}

module.exports.parseCookies = req => {
  const list = {}
  const rc = req.headers.cookie

  rc &&
    rc.split(';').forEach(function (cookie) {
      const parts = cookie.split('=')
      list[parts.shift().trim()] = decodeURI(parts.join('='))
    })

  return list
}
