const http = require('http')
const dotenv = require('dotenv')

dotenv.config({path: './config.env'})

const routes = require('./routes')
const router = require('./router')

process.on('uncaughtException', function (err) {
  // handle the error safely
  console.log('uncaughtException')
  console.error(err.stack)
  console.log(err)
})

const port = process.env.PORT || 5000

const server = http.createServer(async (req, res) => {
  await router(req, res, routes)
})

server.listen(port, () => {
  console.log(`Server is listening on port ${port}`)
})

process.on('unhandledRejection', err => {
  console.log('UNHANDLED REJECTION! Shutting down...')
  console.log(`${err.name} - ${err.message}`)
  server.close(() => {
    process.exit(1)
  })
})
