const fs = require('fs')
const path = require('path')

const response = require('../utils/response')

exports.me = async (req, res) => {
  try {
    const user = JSON.parse(fs.readFileSync(path.join(__dirname, '../data/auth/test-user.json'), 'utf-8'))

    user.password = undefined

    return response.success(res, {user})
  } catch (err) {
    return response.error(res, err)
  }
}
