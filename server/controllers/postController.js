const fs = require('fs')
const path = require('path')

const response = require('../utils/response')

exports.getPosts = async (req, res) => {
  try {
    const posts = JSON.parse(fs.readFileSync(path.join(__dirname, '../data/posts/posts.json'), 'utf-8'))

    return response.success(res, posts)
  } catch (err) {
    return response.error(res, err)
  }
}
