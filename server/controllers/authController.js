const fs = require('fs')
const path = require('path')
const {promisify} = require('util')
const jwt = require('jsonwebtoken')

const {correctEmail, correctPassword} = require('../utils/validate')
const {createSendToken} = require('../utils/common')
const response = require('../utils/response')

exports.login = async (req, res, param, postData) => {
  try {
    const {email, password} = postData

    if (!email || !password) {
      return response.validationError(res, 'Please provide email and password')
    }

    const user = JSON.parse(fs.readFileSync(path.join(__dirname, '../data/auth/test-user.json'), 'utf-8'))

    if (!correctEmail(email, user.email) || !(await correctPassword(password, user.password))) {
      return response.validationError(res, 'Incorrect email or password')
    }

    return createSendToken(res, user)
  } catch (err) {
    return response.error(res, err)
  }
}

exports.protect = async (req, res, fn) => {
  const {authorization} = req.headers

  let token

  if (authorization && authorization.startsWith('Bearer')) {
    token = authorization.split(' ')[1]
  }

  if (!token) {
    return response.validationError(res, 'You are not logged in! Please log in to get access', 401)
  }

  const user = JSON.parse(fs.readFileSync(path.join(__dirname, '../data/auth/test-user.json'), 'utf-8'))

  // Verification token
  const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET)

  if (user.id !== decoded.id) {
    return response.validationError(res, 'The user belonging to this token does no longer exist', 401)
  }

  return fn(req, res)
}
