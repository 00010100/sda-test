const authController = require('./controllers/authController')
const postController = require('./controllers/postController')
const userController = require('./controllers/userController')

const basePath = '/api/v1'

const routes = [
  {
    method: 'GET',
    path: `${basePath}/posts`,
    handler: (req, res) => authController.protect(req, res, postController.getPosts)
  },
  {
    method: 'POST',
    path: `${basePath}/login`,
    handler: authController.login
  },
  {
    method: 'GET',
    path: `${basePath}/user`,
    handler: (req, res) => authController.protect(req, res, userController.me)
  }
]

module.exports = routes
