const fs = require('fs')
const https = require('https')

const importData = () => {
  const writeStream = fs.createWriteStream('./data/posts/posts.json')

  https
    .get('https://jsonplaceholder.typicode.com/posts?_limit=10', res => {
      let data = ''

      // A chunk of data has been recieved
      res
        .on('data', chunk => {
          data += chunk
        })
        .on('end', () => {
          writeStream.write(data)

          // This is here incase any errors occur
          writeStream.on('error', function (err) {
            console.log(err)
          })

          console.log('Data successfully loaded')
        })
    })
    .on('error', err => {
      console.log('Error: ' + err.message)
    })
}

if (process.argv[2] === '--import') {
  importData()
}
