# sda test
###### Simple Node.js server without framework

### Installation

Firstly make sure that you have [Node](https://nodejs.org/en/download/) and [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) installed.

Next clone this repo https://bitbucket.org/00010100/sda-test/src. You can do this by going into your shell of choice and entering
```
git clone https://00010100@bitbucket.org/00010100/sda-test.git
```
Then change into that folder
```
cd sda-test
```

install the necessary packages locally with npm
``` npm install ```

for import source data to server
```
cd server && npm run import:data
```

run server and client container with docker
```
docker-compose up --build
```

### Server routes

POST ```http://localhost:5000/api/v1/login```

```
{"email": "aarav@example.com", "password": "test1234"}
```

GET ```http://localhost:5000/api/v1/posts``` (protected)

GET ```http://localhost:5000/api/v1/user``` (protected)

##
Now visit [`localhost:3000`](http://localhost:3000) from your browser. Now your client should be up and running.