import React, {useEffect, useContext, useState} from 'react'
import {Redirect} from 'react-router-dom'
import useLocalStorage from '../../hooks/useLocalStorage'
import {AuthType} from '../../constants/auth'
import {CurrentUserContext} from '../../contexts/currentUser'

export const Logout = () => {
  const {dispatch} = useContext(CurrentUserContext)
  const [isSuccessfullLogout, setIsSuccessfullLogout] = useState<boolean>(false)
  const {setToken} = useLocalStorage('token')

  useEffect(() => {
    setToken('')
    dispatch({type: AuthType.LOGOUT})
    setIsSuccessfullLogout(true)
  }, [dispatch, setToken])

  return isSuccessfullLogout && <Redirect to="/" />
}
