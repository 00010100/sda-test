import React, {useEffect, useState, useContext} from 'react'
import {Redirect} from 'react-router-dom'
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import useFetch from '../../hooks/useFetch'
import useLocalStorage from '../../hooks/useLocalStorage'
import {CurrentUserContext} from '../../contexts/currentUser'
import {BackendErrorMessages} from '../../components/backendErrorMessages'
import {AuthType} from '../../constants/auth'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& > *': {
        margin: theme.spacing(2)
      }
    }
  })
)

export const Authentication = () => {
  const classes = useStyles()
  const [email, setEmail] = useState<string>('')
  const [password, setPassword] = useState<string>('')
  const [isSuccessfullSubmit, setIsSuccessfullSubmit] = useState<boolean>(false)
  const {dispatch} = useContext(CurrentUserContext)
  const [{response, isLoading, error}, doFetch] = useFetch('/login')
  const {setToken} = useLocalStorage('token')
  const [open, setOpen] = useState<boolean>(false)

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()

    doFetch({method: 'POST', data: {email, password}})
  }

  const handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return
    }

    setOpen(false)
  }

  useEffect(() => {
    if (!response) return

    setToken(response.data.token)
    setIsSuccessfullSubmit(true)
    dispatch({type: AuthType.SET_AUTHORIZED, payload: response.data.user})
  }, [response, setToken, dispatch])

  if (isSuccessfullSubmit) {
    return <Redirect to="/posts" />
  }

  return (
    <div>
      <Typography variant="h2" align="center">
        Login page
      </Typography>
      <BackendErrorMessages backendError={error} />
      <form className={classes.root} noValidate autoComplete="off" onSubmit={handleSubmit}>
        <div>
          <TextField
            onChange={e => setEmail(e.target.value)}
            id="email"
            label="Email"
            fullWidth
            value={email}
            type="email"
            placeholder="Enter your email"
          />
        </div>
        <div>
          <TextField
            onChange={e => setPassword(e.target.value)}
            value={password}
            type="password"
            id="password"
            label="Password"
            fullWidth
            placeholder="Enter your password"
          />
        </div>
        <Button type="submit" variant="contained" color="primary" disabled={isLoading}>
          Login
        </Button>
      </form>
    </div>
  )
}
