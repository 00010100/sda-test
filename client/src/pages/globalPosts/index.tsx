import React, {useEffect, useContext} from 'react'

import useFetch from '../../hooks/useFetch'
import {Posts} from '../../components/posts'
import {Loading} from '../../components/loading'
import {BackendErrorMessages} from '../../components/backendErrorMessages'
import {CurrentUserContext} from '../../contexts/currentUser'

export const GlobalPosts = () => {
  const {state} = useContext(CurrentUserContext)
  const [{response, isLoading, error}, doFetch] = useFetch('/posts')

  useEffect(() => {
    if (!state.isLoggedIn) return

    doFetch()
  }, [doFetch, state.isLoggedIn])

  return (
    <div>
      {isLoading && <Loading />}
      <BackendErrorMessages backendError={error} />
      {!isLoading && !state.isLoggedIn && <p>Login to see posts</p>}
      {!isLoading && !!response && <Posts posts={response.data} />}
    </div>
  )
}
