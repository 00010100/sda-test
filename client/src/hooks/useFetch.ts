import {useEffect, useState, useCallback} from 'react'
import axios from 'axios'
import useLocalStorage from './useLocalStorage'

export default (url: string) => {
  const baseUrl: string = 'http://localhost:5000/api/v1'
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [response, setResponse] = useState(null)
  const [error, setError] = useState<null | string>(null)
  const [options, setOptions] = useState({})
  const {token} = useLocalStorage('token')

  const doFetch = useCallback((options = {}) => {
    setOptions(options)
    setIsLoading(true)
  }, [])

  useEffect(() => {
    const requestOptions = {
      ...options,
      ...{
        headers: {
          authorization: token ? `Bearer ${token}` : '',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': '*',
          'Content-Type': 'application/json'
        }
      }
    }

    if (!isLoading) return

    axios(`${baseUrl}${url}`, requestOptions)
      .then(result => {
        setIsLoading(false)
        setResponse(result.data)
      })
      .catch(error => {
        setIsLoading(false)
        setError(error.response.data.error)

        setTimeout(() => {
          setError(null)
        }, 6000)
      })
  }, [url, options, isLoading, token])

  return [{isLoading, response, error}, doFetch] as any
}
