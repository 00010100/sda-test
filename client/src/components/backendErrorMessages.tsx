import React from 'react'
import Snackbar from '@material-ui/core/Snackbar'
import MuiAlert from '@material-ui/lab/Alert'

export const BackendErrorMessages = ({backendError}: {backendError: string}) => {
  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right'
      }}
      open={!!backendError}
    >
      <MuiAlert variant="filled" severity="error">
        {backendError}
      </MuiAlert>
    </Snackbar>
  )
}
