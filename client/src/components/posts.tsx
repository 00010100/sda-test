import React from 'react'
import {createStyles, Theme, makeStyles} from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import {Post} from '../interfaces/post'
import {Divider} from '@material-ui/core'

interface Props {
  posts: Post[]
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
      position: 'relative'
    },
    li: {
      display: 'block'
    }
  })
)

export const Posts = ({posts}: Props) => {
  const classes = useStyles()

  if (posts.length === 0) {
    return <p>No posts yet</p>
  }

  return (
    <List className={classes.root}>
      {posts.map(post => (
        <ListItem key={post.id} className={classes.li}>
          <ListItemText primary={post.title} />
          <ListItemText secondary={post.body} />
          <Divider />
        </ListItem>
      ))}
    </List>
    // <ul>
    //   {posts.map(post => (
    //     <li key={post.id}>
    //       <h3>{post.title}</h3>
    //       <p>{post.body}</p>
    //     </li>
    //   ))}
    // </ul>
  )
}
