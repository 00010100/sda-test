import React, {useContext} from 'react'
import {Link, NavLink} from 'react-router-dom'
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

import {CurrentUserContext} from '../contexts/currentUser'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    title: {
      flexGrow: 1
    }
  })
)

export const TopBar = () => {
  const classes = useStyles()
  const {state: currentUserState} = useContext(CurrentUserContext)

  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" className={classes.title}>
          <Link to="/">sda-test</Link>
        </Typography>
        {currentUserState.isLoggedIn === false && (
          <Typography variant="h6">
            <NavLink to="/login">Login</NavLink>
          </Typography>
        )}
        {currentUserState.isLoggedIn && (
          <div>
            <Typography variant="h6">
              Welcome, {currentUserState.currentUser && currentUserState.currentUser.name}
            </Typography>
            <Typography variant="body1" align="right">
              <NavLink to="/logout">Log Out</NavLink>
            </Typography>
          </div>
        )}
      </Toolbar>
    </AppBar>
  )
}
