import {useEffect, useContext} from 'react'
import useFetch from '../hooks/useFetch'
import {CurrentUserContext} from '../contexts/currentUser'
import useLocalStorage from '../hooks/useLocalStorage'
import {AuthType} from '../constants/auth'

export const CurrentUserChecker = ({children}: {children: JSX.Element}) => {
  const [{response}, doFetch] = useFetch('/user')

  const {dispatch} = useContext(CurrentUserContext)
  const {token} = useLocalStorage('token')

  useEffect(() => {
    if (!token) {
      dispatch({type: AuthType.SET_UNAUTHORIZED})
      return
    }

    doFetch()
    dispatch({type: AuthType.LOADING})
  }, [token, dispatch, doFetch])

  useEffect(() => {
    if (!response) return

    dispatch({type: AuthType.SET_AUTHORIZED, payload: response.data.user})
  }, [response, dispatch])

  return children
}
