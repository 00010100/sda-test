import React from 'react'
import {Switch, Route} from 'react-router-dom'
import {GlobalPosts} from './pages/globalPosts'
import {MainPage} from './pages/mainPage'
import {Authentication} from './pages/authentication'
import {Logout} from './pages/logout'

export default () => {
  return (
    <Switch>
      <Route path="/" exact component={MainPage} />
      <Route path="/login" component={Authentication} />
      <Route path="/posts" component={GlobalPosts} />
      <Route path="/logout" component={Logout} />
    </Switch>
  )
}
