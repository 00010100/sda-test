import React, {createContext, useReducer} from 'react'
import {AuthState, AuthAction} from '../interfaces/auth'
import {AuthType} from '../constants/auth'

const initialState: AuthState = {
  isLoading: false,
  isLoggedIn: null,
  currentUser: null
}

interface IContextProps {
  state: AuthState
  dispatch: ({type}: {type: string, payload?: any}) => void
}

const reducer = (state: AuthState, {type, payload}: AuthAction) => {
  switch (type) {
    case AuthType.LOADING:
      return {...state, isLoading: true}
    case AuthType.SET_AUTHORIZED:
      return {...state, isLoggedIn: true, isLoading: false, currentUser: payload}
    case AuthType.SET_UNAUTHORIZED:
      return {...state, isLoggedIn: false}
    case AuthType.LOGOUT:
      return {...initialState, isLoggedIn: false}
    default:
      return state
  }
}

export const CurrentUserContext = createContext({} as IContextProps)

export const CurrentUserProvider = (props: any) => {
  const [state, dispatch] = useReducer(reducer, initialState)

  const value = {state, dispatch}

  return <CurrentUserContext.Provider value={value}>{props.children}</CurrentUserContext.Provider>
}
