import {User} from './user'

export interface AuthState {
  isLoading: boolean
  isLoggedIn: null | boolean
  currentUser?: User | null | undefined
}

export interface AuthAction {
  type: string
  payload?: User | null | undefined
}
