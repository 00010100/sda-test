import React from 'react'
import {render} from 'react-dom'
import {BrowserRouter as Router} from 'react-router-dom'
import Container from '@material-ui/core/Container'
import Routes from './routes'
import {TopBar} from './components/topBar'
import {CurrentUserProvider} from './contexts/currentUser'
import {CurrentUserChecker} from './components/currentUserChecked'

import * as serviceWorker from './serviceWorker'

const App = () => (
  <CurrentUserProvider>
    <CurrentUserChecker>
      <Router>
        <Container maxWidth="md">
          <TopBar />
          <Routes />
        </Container>
      </Router>
    </CurrentUserChecker>
  </CurrentUserProvider>
)

render(<App />, document.getElementById('root'))

serviceWorker.unregister()
